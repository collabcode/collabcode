<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Laravel</title>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<!-- Styles -->
<link rel="stylesheet" href="{{ asset('css/bulma.css') }}">
<link rel="stylesheet" href=" {{ asset('css/custom.css') }}">
<link rel="stylesheet" href="{{ asset('css/reset.css') }}">
