<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
      @include('frontend.layout.head')
    </head>
    <body class="hero">
      <header>
        @include('frontend.layout.navbar')
      </header>
        <div class="flex-center position-ref full-height">
            <div class="container">
              <section class="main-content">
              <div class="m-t-20">
                @yield('content')
              </div>
              </section>
            </div>
        </div>
        <footer>
          @include('frontend.layout.footer')
        </footer>
    </body>
</html>
