<div class="top-primary">
  <div class="container is-mobile level">
    <div class="site-logo is-mobile is-tablet level-left">
      <a class="navbar-brand" href="/">
      <img id="brand" src="{!! asset('images/frontend/collab-logo.png') !!}" alt="CollabCode logo">
      </a>
    </div>
    <nav class="menu">
      <ul class="menu-list">
        <li class="menu-link"> <a href="/">BLOG</a> </li>
        <li> <a href="/">FOR COMPANIES</a> </li>
      </ul>
      <a class="button is-outlined">
    <span class="icon github-icon">
      <button type="button" name="button" class=""></button>
    </span>
    <span>Login</span>
  </a>
    </nav>
  </div>
</div>
