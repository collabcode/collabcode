# CollabCode v0.2.0

Para a comunidade, pela comunidade
Discord : [link](https://Discord.gg/invite/FP8k4s7)

## Roadmap features

- [ ] Refactory Laravel
- [ ] Geolocation auto
- [ ] Change the navbar UI  -- v0.2.0-1
- [ ] Create a Hero Banner ( languages program )  -- v0.2.0-2
- [ ] Create a Courses informations   -- v0.2.0-3
- [ ] Create a banner new members logged   -- v0.2.0-4
- [ ] Create a footer   -- v0.2.0-5

Make sure if you don't know Laravel Framework contact our
Discord Administrator or @pannic12 Discord ID:[Lucas#2012] to help you.
